## Desafio Itaú 03
# Consolidação de dados usando framework


>  **WORK IN PROGRESS**   
> Próximos passos:
> * Terminar a guia de dados por categoria
> * Remover os meses sem gastos (zerados) da tabela de meses
> * Refatorar parte da lógica


Esse é o desafio 3 de 3, em que a habilidade com **frameworks Javascript**, **requisições http**, **implementação de um design system** e **programação funcional** é avaliada. Para isso é necessário consumir uma API onde estão os lançamentos de um cartão de credito dentro de um mesmo ano. Instruções de consumo da API [neste link](https://gitlab.com/desafio3/readme-api).   
&nbsp;      
      
### **Links de cada um dos desafios:**

* DESAFIO 01 = HTML + BOOTSTRAP + JAVASCRIPT VANILLA
  * Repositório: https://gitlab.com/anacoxta/desafio-itau-01
  * Deploy: https://desafio-itau-01.netlify.com/
* DESAFIO 2 = REACT + BULMA CSS + STYLED-COMPONENTS
  * Repositório: https://gitlab.com/anacoxta/desafio-itau-02
  * Deploy: https://desafio-itau-02.netlify.com
* **DESAFIO 3 (este) = REACT + MATERIAL-UI + STYLED-COMPONENTS**
  * **Repositório: https://gitlab.com/anacoxta/desafio-itau-03** _(work in progress)_
  * **Deploy: https://desafio-itau-03.netlify.com/**  _(work in progress)_      
&nbsp;   


      
### Cenário Proposto
_Usando a mesma API do desafio anterior é necessário criar o front-end, com aspecto de app, usando seus frameworks de preferência. Será necessário criar um app baseado em seu Design System de preferência, recomendamos o Material Design do Google, esse app deve conter 3 abas que irão agrupar os gastos das seguntes formas: **Lista Geral**, **Por Mês** e **Por Categoria.**_   
&nbsp;      
      
### Requisitos

- [x] O framework utilizado deve ser um framework de Javascript;
- [x] Seu app deve ser responsivo, usando seu Design System de prefêrencia (recomendamos o Material Design); 
- [x] A tela do seu app deve conter 3 abas [(ver exemplo)](https://gitlab.com/desafio3/desafio-final/-/blob/master/Experiencia/Experiencia.mov)
    - [x] _Lista Geral_,
    - [x] _Por Mês_ e
    - [ ] _Por Categoria_;
- [ ] Só devem ser exibidos os meses em que houveram lançamentos;
- [x] Os meses e categorias devem ser exibidos por escrito sempre.
&nbsp;      
> **Importante: para otimizar a exibição dos dados da Tabela Geral em telas menores, abaixo do breakpoint de 480px os meses são exibidos "encurtados".**    
> *Exemplo: "Jan" em vez de "Janeiro".*   

&nbsp;      
&nbsp;      
      
     
      
# Instruções

Para rodar o código localmente...
1) [Clone](https://help.github.com/pt/github/creating-cloning-and-archiving-repositories/cloning-a-repository) este repositório em sua máquina
2) Abra a linha de comando e acesse (através dela) a pasta do repositório local
3) Use o comando **`npm install`** para instalar as dependências localmente
4) Use o comando **`npm start`** para rodar o aplicativo em modo de desenvolvimento, acessível através do endereço http://localhost:3000 em seu navegador...     
  ...ou use **`npm run build`** para "empacotar" o aplicativo para produção (na pasta _build_).   
&nbsp;   
&nbsp;   


### **Observação**:
Os requisitos mínimos para rodar este projeto são:
* Ter [Node.js](https://nodejs.org/en/download/) instalado localmente na máquina
* Um navegador de sua preferência