import React, { useContext } from 'react';
import styled from 'styled-components';
import MUIDataTable from 'mui-datatables';
import { Context } from '../context/Context';

const TableMes = () => {
  // Makes the Context accessible
  const [state, setState] = useContext(Context);

  let categData = {
    Janeiro: 0,
    Fevereiro: 0,
    Março: 0,
    Abril: 0,
    Maio: 0,
    Junho: 0,
    Julho: 0,
    Agosto: 0,
    Setembro: 0,
    Outubro: 0,
    Novembro: 0,
    Dezembro: 0,
  };

  for (let obj of state.treated) {
    categData[obj[2]] = categData[obj[2]] + obj[3];
    categData[obj[2]] = categData[obj[2]];
  }

  let newAr = [];
  for (let m in categData) {
    if (categData[m] > 0) {
      categData[m] = categData[m].toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
      newAr.push([m, categData[m]]);
    }
  }

  let columns = ['Mês', 'Valor Gasto'];
  let data = newAr;

  const options = {
    filterType: 'dropdown',
    responsive: 'scroll',
    pagination: false,
    disableToolbarSelect: true,
    elevation: 0,
    filter: false,
    download: false,
    print: false,
    viewColumns: false,
    selectableRows: 'none',
    search: false
  };

  return <DataTable title={'Lançamentos - Consolidado por mês'} data={data} columns={columns} options={options} />;
};

const DataTable = styled(MUIDataTable)`
  margin-top: 0.5rem;
  height: 100%;
`;

export default TableMes;
