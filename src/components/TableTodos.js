import React, { useContext } from 'react';
import styled from 'styled-components';
import MUIDataTable from 'mui-datatables';
import { Context } from '../context/Context';

const TableTodos = () => {
  // Makes the Context accessible
  const [state, setState] = useContext(Context);

  let newArray = [];

  for (let item of state.treated) {
    let newItemArray = [];
    newItemArray.push(item[0]);
    newItemArray.push(item[3]);
    newArray.push(newItemArray);
  }

  let columns = ['Origem', 'Valor'];
  let data = newArray;

  const options = {
    filterType: 'dropdown',
    responsive: 'scroll',
    pagination: false,
    disableToolbarSelect: true,
    elevation: 0,
    filter: false,
    download: false,
    print: false,
    viewColumns: false,
    selectableRows: 'none',
  };

  return <DataTable title={'Todos os lançamentos'} data={data} columns={columns} options={options} />;
};

const DataTable = styled(MUIDataTable)`
  margin-top: 0.5rem;
  height: 100%;
`;

export default TableTodos;
