import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const ContentPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component='div'
      role='ContentPanel'
      hidden={value !== index}
      id={`nav-ContentPanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}>
      {value === index && <Box p={0}>{children}</Box>}
    </Typography>
  );
};

ContentPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

export default ContentPanel;
