import React, { useContext } from 'react';
import { Context } from '../context/Context';

import Tabs from '@material-ui/core/Tabs';
import Paper from '@material-ui/core/Paper';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import TodayIcon from '@material-ui/icons/Today';
import StoreIcon from '@material-ui/icons/Store';

import LinkTab from './LinkTab';
import Loader from './Loader';
import { useWitdh } from '../actions/Actions';

// Sets some accessibilty features
const a11yProps = (index) => {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-ContentPanel-${index}`,
  };
};

const NavTabs = (props) => {
  // Makes the Context accessible
  const [state, setState] = useContext(Context);

  // Gets page width
  let width = useWitdh();

  // Deals with tab switching
  const handleChange = (event, newValue) => {
    setState({ focus: newValue });
  };

  if (width < 480) {
    return (
      <div>
        <Paper square>
          <Tabs
            variant='fullWidth'
            value={state.focus}
            onChange={handleChange}
            indicatorColor='secondary'
            textColor='secondary'
            aria-label='nav tabs example'>
            <LinkTab label='Todos' href='/todos' {...a11yProps(0)} />
            <LinkTab label='Mês' href='/mes' {...a11yProps(1)} />
            <LinkTab label='Categ.' href='/spam' {...a11yProps(2)} />
          </Tabs>
        </Paper>
        {state.isLoading ? <Loader /> : props.children}
      </div>
    );
  } else if (width < 650) {
    return (
      <div>
        <Paper square>
          <Tabs
            variant='fullWidth'
            value={state.focus}
            onChange={handleChange}
            indicatorColor='secondary'
            textColor='secondary'
            aria-label='nav tabs example'>
            <LinkTab label='Todos' href='/todos' {...a11yProps(0)} />
            <LinkTab label='Mês' href='/mes' {...a11yProps(1)} />
            <LinkTab label='Categoria' href='/spam' {...a11yProps(2)} />
          </Tabs>
        </Paper>
        {state.isLoading ? <Loader /> : props.children}
      </div>
    );
  } else {
    return (
      <div>
        <Paper square>
          <Tabs
            centered
            value={state.focus}
            onChange={handleChange}
            indicatorColor='secondary'
            textColor='secondary'
            aria-label='nav tabs example'>
            <LinkTab icon={<ShoppingCartIcon />} label='Todos' href='/todos' {...a11yProps(0)} />
            <LinkTab icon={<TodayIcon />} label='Mês' href='/mes' {...a11yProps(1)} />
            <LinkTab icon={<StoreIcon />} label='Categoria' href='/spam' {...a11yProps(2)} />
          </Tabs>
        </Paper>
        {state.isLoading ? <Loader /> : props.children}
      </div>
    );
  }
};

export default NavTabs;
