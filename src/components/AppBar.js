import React from 'react';
import {default as MaterialAppBar} from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const AppBar = () => {
  return (
    <div>
      <MaterialAppBar position='static'>
        <Toolbar>
          <Typography variant='h6'>AppLançamentos</Typography>
        </Toolbar>
      </MaterialAppBar>
    </div>
  );
};

export default AppBar;
