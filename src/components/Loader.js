import React from 'react';
import styled from 'styled-components';
import { ReactComponent as LoaderIcon } from '../assets/loader.svg';

const Loader = () => {
  return (
    <Div>
      <LoaderIcon />
    </Div>
  );
};

const Div = styled.div`
  width: 100%;
  height: 100%;
  align-self: center;
  text-align: center;
`;

export default Loader;
