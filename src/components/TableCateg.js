import React, { useContext } from 'react';
import styled from 'styled-components';
import MUIDataTable from 'mui-datatables';
import { Context } from '../context/Context';

const TableCateg = () => {
  // Makes the Context accessible
  const [state, setState] = useContext(Context);

  let columns = ['Categoria', 'Valor Gasto'];
  let data = state.treated

  const options = {
    filterType: 'dropdown',
    responsive: 'scroll',
    pagination: false,
    disableToolbarSelect: true,
    elevation: 0,
    filter: false,
    download: false,
    print: false,
    viewColumns: false,
    selectableRows: 'none',
  };

  return <DataTable title={'Lançamentos - Consolidado por categoria'} data={data} columns={columns} options={options} />;
};

const DataTable = styled(MUIDataTable)`
  margin-top: 0.5rem;
  height: 100%;
`;

export default TableCateg;
