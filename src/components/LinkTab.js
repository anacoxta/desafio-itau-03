import React from 'react';
import Tab from '@material-ui/core/Tab';

const LinkTab = (props) => {
  return (
    <Tab
      component='a'
      icon={props.icon}
      onClick={(event) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
};

export default LinkTab;
