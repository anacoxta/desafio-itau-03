import { useEffect, useContext } from 'react';
import { Context } from '../context/Context';
import { treatData } from '../actions/Actions';
import { renameCat, listCat } from '../actions/Actions';

/* ==================================================================
1) Calls the context (global state = single source of truth)
2) This specific useEffect hook is called only once, after mounting
3) Fetches from the URL passed
4) THEN, if fetch is successful: parses the response as json
         if fetch is unsuccessful: throws error
5) THEN, sends the response to the context
6) OR CATCHES the error and updates the state
================================================================== */

const API = () => {
  const endpointLancamentos = 'https://desafio-it-server.herokuapp.com/lancamentos';
  const endpointCategorias = 'https://desafio-it-server.herokuapp.com/categorias';

  const [state, setState] = useContext(Context);
  useEffect(() => {
    fetch(endpointLancamentos)
      .then((response) => {
        if (response.ok) {
          return (response = response.json());
        } else {
          throw Error('Não foi possível carregar os dados do servidor');
        }
      })
      .then((data) => {
        const treated = treatData(data);
        setState({ treated: treated });
        return fetch(endpointCategorias);
      })
      .then((response) => {
        if (response.ok) {
          return (response = response.json());
        } else {
          throw Error('Não foi possível carregar os dados do servidor');
        }
      })
      .then((data) => {
        setState({ cat: data, isLoading: false });
      })
      .then((state) => {
        listCat(state.cat);
        renameCat(state.treated);
      })
      .catch((error) => {
        setState({ error: error, isLoading: false });
      });
  }, []);
};

export default API;
