import React, { useContext, useEffect } from 'react';
import styled from 'styled-components';

import { Context } from '../context/Context';
import API from '../services/API';
import { renameData, renameCat, listCat } from '../actions/Actions';

import AppBar from '../components/AppBar';
import NavTabs from '../components/NavTabs';
import ContentPanel from '../components/ContentPanel';

import TableTodos from '../components/TableTodos';
import TableMes from '../components/TableMes';
// import TableCateg from '../components/TableCateg'; //work in progress

const Home = () => {
  // Makes the Context accessible
  const [state, setState] = useContext(Context);

  // Calls both endpoints of the API and puts data into Context
  API();

  useEffect(() => {
    renameData(state.treated);
  }, [state.treated]);

  useEffect(() => {
    listCat(state.cat);
    renameCat(state.treated);
  }, [state.cat]);

  console.log('state home', state);
  return (
    <PageContainer>
      <AppBar />
      <NavTabs>
        <ContentPanel value={state.focus} index={0}><TableTodos /></ContentPanel>
        <ContentPanel value={state.focus} index={1}><TableMes /></ContentPanel>
        <ContentPanel value={state.focus} index={2}>WORK IN PROGRESS{/* <TableCateg /> */}</ContentPanel>
      </NavTabs>
    </PageContainer>
  );
};

const PageContainer = styled.div`
  max-width: 100vw;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

export default Home;
