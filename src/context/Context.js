import React, { createContext, useState } from 'react';

// State merger hook
// (this way I won't need to destructure every time I use setState)
const useMergeState = (initialState) => {
  const [state, setState] = useState(initialState);
  const setMergedState = (newState) => setState((prevState) => Object.assign({}, prevState, newState));
  return [state, setMergedState];
};

const Context = createContext([{}, () => {}]);

// Single source of truth
const ContextProvider = (props) => {
  const [state, setState] = useMergeState({
    isLoading: true,
    error: false,
    focus: 0,
    lanc: '',
    cat: '',
    treated: '',
    renamed: '',
  });

  return <Context.Provider value={[state, setState]}>{props.children}</Context.Provider>;
};

export { Context, ContextProvider };
