import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import { ContextProvider } from "./context/Context";
import Home from './pages/Home';


ReactDOM.render(
  <React.StrictMode>
    <ContextProvider>
      <Home />
    </ContextProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
