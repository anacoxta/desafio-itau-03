import { useState, useEffect } from 'react';

export const replaceMonthName = (num) => {
  switch (num) {
    case 1:
      return "Janeiro";
    case 2:
      return 'Fevereiro';
    case 3:
      return 'Março';
    case 4:
      return 'Abril';
    case 5:
      return 'Maio';
    case 6:
      return 'Junho';
    case 7:
      return 'Julho';
    case 8:
      return 'Agosto';
    case 9:
      return 'Setembro';
    case 10:
      return 'Outubro';
    case 11:
      return 'Novembro';
    case 12:
      return 'Dezembro';
    default:
      return '';
  }
};

// CREDITS FOR THIS GREAT HOOK:
// https://dev.to/vitaliemaldur/resize-event-listener-using-react-hooks-1k0c
export const getWidth = () => window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
export const useWitdh = () => {
  // save current window width in the state object
  let [width, setWidth] = useState(getWidth());

  useEffect(() => {
    // timeoutId for debounce mechanism
    let timeoutId = null;
    const resizeListener = () => {
      // prevent execution of previous setTimeout
      clearTimeout(timeoutId);
      // change width from the state object after 150 milliseconds
      timeoutId = setTimeout(() => setWidth(getWidth()), 150);
    };
    // set resize listener
    window.addEventListener('resize', resizeListener);

    // clean up function
    return () => {
      // remove resize listener
      window.removeEventListener('resize', resizeListener);
    };
  }, []);

  return width;
};

// ---

export const treatData = (data) => {
  let newArray = [];
  for (let d of data) {
    let transactionArray = [];
    transactionArray = [d.origem, d.categoria, d.mes_lancamento, d.valor];
    newArray.push(transactionArray);
  }
  return newArray
};

export const renameData = (data) => {
  for (let d in data) {
    // data[d][1] = replaceCategoryName(data[d][1])
    data[d][2] = replaceMonthName(data[d][2])
  }
}

let catNames = []

export const renameCat = (data) => {
  for (let d in data) {
    if (data[d][1] === 1) data[d][1] = catNames[0]
    if (data[d][1] === 2) data[d][1] = catNames[1]
    if (data[d][1] === 3) data[d][1] = catNames[2]
    if (data[d][1] === 4) data[d][1] = catNames[3]
    if (data[d][1] === 5) data[d][1] = catNames[4]
    if (data[d][1] === 6) data[d][1] = catNames[5]
  }
}

export const listCat = (data) => {
  for (let d in data) {
    catNames.push(data[d].nome)
  }
}

